#!/usr/bin/python
import sys
from random import shuffle
NUM_KEYS = int(sys.argv[1])
BLOAT = int(sys.argv[2])
BLOAT = (BLOAT*NUM_KEYS)/100
keys = [x for x in range(NUM_KEYS)]
shuffle(keys)
for key in range(BLOAT):
	s = str(keys[key])
	print ("*2\r\n$3\r\nDEL\r\n$10\r\n"+s.zfill(10)+"\r\n") ,
