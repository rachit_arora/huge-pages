#!/usr/bin/python
import sys
NUM_KEYS = int(sys.argv[1])
SIZE = int(sys.argv[2])
val = ''
for i in range(SIZE-10):
	val = val + '.'

output = open("add.txt","w")
for key in range(NUM_KEYS):
	output.write("*3\r\n$3\r\nSET\r\n$10\r\n")
	s = str(key)
	output.write(s.zfill(10))
	output.write("\r\n$" + str(SIZE-10) + "\r\n"+val+"\r\n")

output.close()
