#!/usr/bin/python
import sys
from random import shuffle
NUM_KEYS = int(sys.argv[1])
BLOAT = int(sys.argv[2])
BLOAT = (BLOAT*NUM_KEYS)/100
keys = [x for x in range(NUM_KEYS)]
shuffle(keys)
output = open("remove.txt","w")
for key in range(BLOAT):
	output.write("*2\r\n$3\r\nDEL\r\n$10\r\n")
	s = str(keys[key])
	output.write(s.zfill(10)+"\r\n")

output.close()
