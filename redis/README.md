### Some experiments and benchmarks with Redis application
---
### Usage

1. Memory Bloat and Performance on key object addition, removal.  
  Use redis_bloat.sh as follows :

  ```
  ./redis_bloat.sh num size bloat
  ```
  
  * num : Number of keys-value pairs to be added (Eg. 1000 for a thousand pairs, maximum (10^10-1) ).
  * size : Size of an individual key-value object being added (Eg. 4096 for 4 kB objects, minimum 10B).
  * bloat : Percentage of the keys to be randomly removed. (Eg. 10 for 10%)
  * NOTE : For a given size, we declare a key of size 10 characters (10B) and a string value made up of (size-10) dots ('.') .

  * Per command execution latency saved in ```latency_data.txt```. The log records unique id, timestamp, execution time, command type, etc respectively for each executed command and can be used to measure tail latency of a given command.
