#!/bin/bash

#echo "NOTE : Generating Redis command files"
#python generate_key_value.py $1 $2
#python remove_keys.py $1 $3

redis-cli slowlog reset
redis-cli config set slowlog-log-slower-than 0
redis-cli config set slowlog-max-len 10000000

START=$(date +%s.%N)
echo "NOTE : Adding key-value pairs to Redis"
#cat add.txt | redis-cli --pipe
python generate_key_value_pipe.py $1 $2 | redis-cli --pipe
echo
MID=$(date +%s.%N)
echo "NOTE : Randomly removing $3 percent objects"
#cat remove.txt | redis-cli --pipe
python remove_keys_pipe.py $1 $3 | redis-cli --pipe
END=$(date +%s.%N)
echo

echo "NOTE : Object removal complete"
echo "--------------------"
TOTAL=$(echo "$END - $START" | bc)
ADDING=$(echo "$MID - $START" | bc)
REMOVING=$(echo "$END - $MID" | bc)
echo "$TOTAL seconds to complete"
echo "$ADDING seconds to add keys"
echo "$REMOVING seconds to remove keys"
echo "--------------------"
echo
#echo "NOTE : Removing temporary command files"
#rm add.txt remove.txt

#Write latency data to file in the format
#1) 1) (integer) 14
#   2) (integer) 1309448221
#   3) (integer) 15
#   4) 1) "ping"
#2) 1) (integer) 13
#   2) (integer) 1309448128
#   3) (integer) 30
#   4) 1) "slowlog"
#      2) "get"
#      3) "100"
#The 3) entry gives the time taken for execution in microseconds

ENTRIES=$(redis-cli slowlog len)
echo "NOTE : $ENTRIES latency log entries being written to latency_data.txt"
redis-cli slowlog get $ENTRIES > latency_data.txt

echo "NOTE : Execution Complete. Exiting."
